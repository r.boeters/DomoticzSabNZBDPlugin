"""
<plugin key="SabNZBD" name="SabNZBD" author="mastair" version="0.0.1" externallink="https://sabnzbd.org/">
    <params>
        <param field="Address" label="IP Address" width="200px" required="true" default="127.0.0.1"/>
        <param field="Port" label="Port" width="30px" required="true" default="8080"/>
        <param field="Mode5" label="API Key" width="200px" required="true"/>
        <param field="Mode6" label="Polling interval (s)" width="40px" required="true" default="10"/>
    </params>
</plugin>
"""
import Domoticz
import json
import urllib
import urllib.request
import urllib.parse

#variables
defaultPollingInterval = 10
unitPaused = 1
unitSpeed = 2
unitMBLeft = 3
unitTimeLeft = 4

def onStart():
    Domoticz.Debug("onStart called")
    
    if ("SabNZBD" not in Images):
        Domoticz.Image('SabNZBD.zip').Create()
        
    if (len(Devices) == 0):
        #Create devices
        Domoticz.Device(Name="Paused", Unit=unitPaused, TypeName="Switch", Image=Images["SabNZBD"].ID).Create()
        Domoticz.Device(Name="Download speed", Unit=unitSpeed, TypeName="Custom", Options={"Custom": "1;MB/s"}, Image=Images["SabNZBD"].ID).Create()
        Domoticz.Device(Name="Time Left", Unit=unitTimeLeft, TypeName="Text", Image=Images["SabNZBD"].ID).Create()
        Domoticz.Device(Name="Download left", Unit=unitMBLeft, TypeName="Custom", Options={"Custom": "1;MB"}, Image=Images["SabNZBD"].ID).Create()
        
    Domoticz.Heartbeat(pollingInterval())
    return True

def onHeartbeat():
    Domoticz.Debug("onHeartBeat called")
    url = 'http://%s:%s/sabnzbd/api?output=json&apikey=%s&mode=qstatus' % (Parameters["Address"],  Parameters["Port"], Parameters["Mode5"])

    # Call SabNZBD API
    try:
        request = urllib.request.urlopen(url)
        data = json.loads(request.read().decode('utf-8'))
    except urllib.error.HTTPError as e:
        Domoticz.Error("Failed to call SabNZBD API: %s" %str(e.code))
        return
    except urllib.error.URLError as e:
        Domoticz.Error("Failed to call SabNZBD API: %s" %str(e.reason))
        return
        
    if "error" in data:
        Domoticz.Error("Received error from SabNZBD API: %s" % data["error"])
        return

    # Calculate values
    paused = 1 if (data["paused"] == True) else 0
    speed = str(round(float(data["kbpersec"])/1024.0, 2))
    mbleft = str(round(float(data["mbleft"]), 2))
    timeleft = data["timeleft"]

    # Updated paused sensor
    if unitPaused in Devices:
        if Devices[unitPaused].nValue != paused:
            Devices[unitPaused].Update(nValue = paused, sValue = str())

    # Update speed sensor
    if unitSpeed in Devices:
        if Devices[unitSpeed].sValue != speed:
            Devices[unitSpeed].Update(nValue = int(), sValue = speed)

    # Update mbleft sensor
    if unitMBLeft in Devices:
        if Devices[unitMBLeft].sValue != mbleft:
            Devices[unitMBLeft].Update(nValue = int(), sValue = mbleft)

    # Update timeleft sensor
    if unitTimeLeft in Devices:
        if Devices[unitTimeLeft].sValue != timeleft:
            Devices[unitTimeLeft].Update(nValue = int(), sValue = timeleft)
    
    return True

def onCommand(Unit, Command, Level, Hue):
    Domoticz.Debug("onCommand called")
    if Unit == unitPaused:
        pauseSabNZBD(Command)
        
    return True

def pauseSabNZBD(Paused):
    Domoticz.Log("Set SabNZBD Pause state to %s" %Paused)
    mode = "resume"
    newNValue = 0
    
    if Paused == "On":
        mode = "pause"
        newNValue = 1

    # Update sensor
    if unitPaused in Devices:
        if Devices[unitPaused].nValue != newNValue:
            Devices[unitPaused].Update(nValue = newNValue, sValue = str())

    # Call SabNZBD API
    try:
        url = 'http://%s:%s/sabnzbd/api?output=json&apikey=%s&mode=%s' % (Parameters["Address"],Parameters["Port"], Parameters["Mode5"], mode)
        request = urllib.request.urlopen(url)
        data = json.loads(request.read().decode('utf-8'))
    except urllib.error.HTTPError as e:
        Domoticz.Error("Failed to call SabNZBD API: %s" %str(e.code))
        return
    except urllib.error.URLError as e:
        Domoticz.Error("Failed to call SabNZBD API: %s" %str(e.reason))
        return
    
    if "error" in data:
        Domoticz.Error("Received error from SabNZBD API: %s" % data["error"])
        return

####### GETTERS FOR PARAMETERS ########
def pollingInterval():
    try:
        return int(Parameters["Mode6"])
    except:
        Domoticz.Error("Error converting polling interval to integer, using %s seconds as polling interval" %defaultPollingInterval)
        return defaultPollingInterval

############## NOT USED ##############
def onStop():
    return True

def onConnect(Status, Description):
    return True

def onMessage(Data, Status, Extra):
    return True

def onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile):
    return True

def onDisconnect():
    return True

