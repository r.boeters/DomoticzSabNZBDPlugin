# DomoticzSabNZBDPlugin
SabNZBD Python plugin for Domoticz Home Automation

What you get:
  - Sensor with pause state (you can control SabNZBD with that as well)
  - Sensor with download speed MB/s
  - Sensor with time left to finish downloading
  - Sensor with data left to download

Prerequisites:
  - SabNZBD, really!?
  - urllib package (pip install urllib if you ain't have it)

Installation and configuration:
  - Put complete folder from zip in Domoticz plugins folder
  - Add SabNZBD hardware device with the right configuration
